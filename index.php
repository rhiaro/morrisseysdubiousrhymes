<!doctype html>
<html>
<head>
    <title>Morrissey's Dubious Rhymes</title>
    <style>
        body {
            font-family: "Courier New", monospace;
            font-size: 67.5%;
            text-align: center;
            background-color: #cccccc;
        }
        h1 {
            color: #dddddd;
            font-weight: bold;
            font-size: 4em;
        }
        p {
            font-size: 2em;
            color: #2d2d2d;
        }
        p.invis {
            color: #cccccc;
            font-weight: bold;
            font-size: 4em;
            padding: 0; margin: 0;
            margin-bottom: -0.6em;
            margin-top: -0.6em;
        }
        div {
            padding-top: 2em; padding-bottom: 2em;
        }
        div:hover {
            background-color: #dddddd;
        }
        div.form:hover {
            background-color: #cccccc;
        }
        hr {
            border: 2px solid #dddddd;
        }
        textarea, input {
            width: 50%;
            border: none;
            background-color: #dddddd;
            font-size: 2em; text-align: center;
        }
        textarea {
            height: 4em;
        }
        input {
            font-size: 1em;
            padding: 0.4em;
        }
    </style>
</head>
<body>
    <h1>Morrissey's Dubious Rhymes</h1>
    <hr/>
    <div>
        <p>so i broke into the palace</p>
        <p>with a sponge and a rusty <strong>spanner</strong></p>
        <p>she said i know you and you cannot sing</p>
        <p>i said that's nothing you should hear me play <strong>piano</strong></p>
        <p class="invis">The Queen is Dead</p>
    </div>
    <hr/>
    <div>
        <p>so how did i end up attached to this <strong>person</strong></p>
        <p>when his sense of humour gets gradually <strong>worser</strong></p>
        <p class="invis">Driving Your Girlfriend Home</p>
    </div>
    <hr/>
    <div>
        <p>i was <strong>bored</strong></p>
        <p>in a <strong>fjord</strong></p>
        <p class="invis">Scandinavia</p>
    </div>
    <hr/>
    <div class="form">
        <form method="post" action="/lyrics">
            <p><textarea name="lyrics"></textarea></p>
            <p><input type="submit" name="sub" value="send more dubious rhymes" /></p>
        </form>
    </div>
</body>
</html>